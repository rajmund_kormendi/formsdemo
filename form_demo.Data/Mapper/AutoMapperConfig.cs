﻿namespace form_demo.Data.Mapper
{
    using AutoMapper;
    using form_demo.Data.Entities;
    using forms_demo.BaseTypes.Entities;

    public static class AutoMapperConfig
    {
        public static IMapper Mapper;

        public static void RegisterMappings()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<UserDto, User>();
            });

            Mapper = new Mapper(config);
        }
    }
}
