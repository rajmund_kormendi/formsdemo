﻿namespace form_demo.Data.LocalService.Interface
{
    using System.Threading.Tasks;
    using forms_demo.BaseTypes.Entities;

    public interface ILocalStorageService
    {
        /// <summary>
        /// Saves a User into the local database
        /// </summary>
        /// <param name="user">User to save</param>
        /// <returns></returns>
        Task CreateUserAsync(User user); 

        /// <summary>
        /// Gets the first User from the local database
        /// </summary>
        /// <returns></returns>
        Task<User> GetUserAsync();

        /// <summary>
        /// Deletes all user from the local database
        /// </summary>
        /// <returns></returns>
        Task DeleteAllUserAsync();
    }
}
