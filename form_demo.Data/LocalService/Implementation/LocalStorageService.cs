﻿namespace form_demo.Data.LocalService.Implementation
{
    using System.Collections.Generic;
    using System.Reactive.Linq;
    using System.Threading.Tasks;
    using Akavache;
    using form_demo.Data.Entities;
    using form_demo.Data.LocalService.Interface;
    using form_demo.Data.Mapper;
    using forms_demo.BaseTypes.Entities;

    public class LocalStorageService : ILocalStorageService
    {
        private const string userKey = "user";
        public LocalStorageService()
        {
        }

        public async Task CreateUserAsync(User user)
        {
            var userDto = AutoMapperConfig.Mapper.Map<UserDto>(user);
            await BlobCache.LocalMachine.InsertObject(userKey, userDto);
        }

        public async Task DeleteAllUserAsync()
        {
            await BlobCache.LocalMachine.InvalidateAll();
        }

        public async Task<User> GetUserAsync()
        {
            try
            {
                var userDto = await BlobCache.LocalMachine.GetObject<UserDto>(userKey);
                return AutoMapperConfig.Mapper.Map<User>(userDto);
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }
    }
}
