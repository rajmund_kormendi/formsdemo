﻿namespace form_demo.Data.Entities
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}
