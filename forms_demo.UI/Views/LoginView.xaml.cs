﻿namespace forms_demo.UI.Views
{
    using MvvmCross.Forms.Views;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginView : MvxContentPage
    {
        public LoginView()
        {
            InitializeComponent();
        }
    }
}