﻿namespace forms_demo.iOS
{
    using MvvmCross.Forms.Platforms.Ios.Core;

    public class Setup : MvxFormsIosSetup<MvxApp, App>
    {
        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();
        }
    }
}