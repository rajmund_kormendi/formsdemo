﻿namespace forms_demo.Droid
{
    using Android.App;
    using Android.Content.PM;
    using MvvmCross.Forms.Platforms.Android.Views;

    [Activity(Label = "forms_demo", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : MvxFormsAppCompatActivity<Setup, MvxApp, App>
    {
        
    }
}