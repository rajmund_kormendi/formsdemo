﻿namespace forms_demo
{
    using Akavache.Sqlite3;
    using Xamarin.Forms.Internals;

    public static class LinkerPreserve
    {
        [Preserve]
        static LinkerPreserve()
        {
            var persistentName = typeof(SQLitePersistentBlobCache).FullName;
            var encryptedName = typeof(SQLiteEncryptedBlobCache).FullName;
        }
    }
}
