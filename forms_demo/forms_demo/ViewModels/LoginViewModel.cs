﻿namespace forms_demo.ViewModels
{
    using System.Threading.Tasks;
    using form_demo.Data.LocalService.Interface;
    using forms_demo.BaseTypes.Entities;
    using MvvmCross.Commands;
    using MvvmCross.Navigation;

    public class LoginViewModel : BaseViewModel
    {
        #region Fields

        private string userName;
        private string password;

        #endregion

        #region Properties

        public virtual string UserName
        {
            get => userName?.Trim();
            set
            {
                SetProperty(ref userName, value);
                RaisePropertyChanged(nameof(IsLoginButtonEnabled));
            }
        }

        public virtual string Password
        {
            get => password?.Trim();
            set
            {
                SetProperty(ref password, value);
                RaisePropertyChanged(nameof(IsLoginButtonEnabled));
            }
        }

        public bool IsLoginButtonEnabled => UserName?.Length > 0 && Password?.Length > 0;

        public virtual IMvxAsyncCommand LoginCommand { get; private set; }

        #endregion

        #region Constructor

        public LoginViewModel(IMvxNavigationService navigationService, ILocalStorageService localStorageService)
            : base(navigationService, localStorageService)
        {
            LoginCommand = new MvxAsyncCommand(ExecuteLoginCommand);
        }

        #endregion

        #region Mvx overrides

        public override void ViewAppeared()
        {
            base.ViewAppeared();
            UserName = string.Empty;
            Password = string.Empty;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
        }

        #endregion

        #region Private methods

        private bool AreCredentialsValid() => string.Equals(UserName, "a") && string.Equals(Password, "a");

        private async Task ExecuteLoginCommand()
        {
            if (AreCredentialsValid())
            {
                var user = new User { Name = UserName, IsAuthenticated = true };
                await localStorageService.CreateUserAsync(user);
                await navigationService.Navigate<HomeViewModel>();
                return;
            }

            await navigationService.Navigate<MessageViewModel>();
        }

        #endregion
    }
}
