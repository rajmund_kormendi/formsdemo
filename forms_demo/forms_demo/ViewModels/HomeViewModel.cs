﻿namespace forms_demo.ViewModels
{
    using System.Threading.Tasks;
    using form_demo.Data.LocalService.Interface;
    using forms_demo.BaseTypes.Entities;
    using MvvmCross.Commands;
    using MvvmCross.Navigation;

    public class HomeViewModel : BaseViewModel
    {
        #region Fields

        private User user;

        #endregion

        #region Properties

        public User User
        {
            get => user;
            set => SetProperty(ref user, value);
        }

        public IMvxAsyncCommand LogoutCommand { get; private set; }

        #endregion

        #region Constructor

        public HomeViewModel(IMvxNavigationService navigationService, ILocalStorageService localStorageService)
            : base(navigationService, localStorageService)
        {
            LogoutCommand = new MvxAsyncCommand(ExecuteLogoutCommand);
        }

        #endregion

        #region Mvx overrides

        public async override Task Initialize()
        {
            await base.Initialize();
            User = await localStorageService.GetUserAsync();
        }

        #endregion

        #region Private methods

        private async Task ExecuteLogoutCommand()
        {
            await localStorageService.DeleteAllUserAsync();
            await ExecuteBackCommand();
        }

        #endregion

    }
}
