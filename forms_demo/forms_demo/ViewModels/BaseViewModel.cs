﻿namespace forms_demo.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using form_demo.Data.LocalService.Interface;
    using MvvmCross.Commands;
    using MvvmCross.Navigation;
    using MvvmCross.ViewModels;

    public class BaseViewModel : MvxViewModel
    {
        #region Fields

        protected readonly IMvxNavigationService navigationService;
        protected readonly ILocalStorageService localStorageService;

        #endregion

        #region Properties

        public IMvxAsyncCommand BackCommand { get; private set; }

        #endregion

        #region Constuctor

        public BaseViewModel(IMvxNavigationService navigationService, ILocalStorageService localStorageService)
        {
            this.navigationService = navigationService ?? throw new ArgumentNullException(nameof(navigationService));
            this.localStorageService = localStorageService ?? throw new ArgumentNullException(nameof(localStorageService));

            BackCommand = new MvxAsyncCommand(ExecuteBackCommand);
        }

        #endregion

        #region Protected methods

        protected virtual Task ExecuteBackCommand()
        {
            return navigationService.Close(this);
        }

        #endregion

    }
}
