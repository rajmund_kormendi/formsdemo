﻿namespace forms_demo.ViewModels
{
    using System.Threading.Tasks;
    using form_demo.Data.LocalService.Interface;
    using MvvmCross.Navigation;

    public class SplashViewModel : BaseViewModel
    {
        #region Constructor

        public SplashViewModel(IMvxNavigationService navigationService, ILocalStorageService localStorageService)
            : base(navigationService, localStorageService)
        {
        }

        #endregion

        #region Mvx overrides

        public override async void ViewAppeared()
        {
            base.ViewAppeared();

            if (await CheckForAuthenticatedUser())
            {
                await navigationService.Navigate<HomeViewModel>();
                return;
            }

            await navigationService.Navigate<LoginViewModel>();
        }

        #endregion

        #region Private methods

        private async Task<bool> CheckForAuthenticatedUser()
        {
            var user = await localStorageService.GetUserAsync();

            return user != null && user.IsAuthenticated;
        }

        #endregion
    }
}
