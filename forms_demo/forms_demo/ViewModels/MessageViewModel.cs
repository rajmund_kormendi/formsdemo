﻿namespace forms_demo.ViewModels
{
    using form_demo.Data.LocalService.Interface;
    using MvvmCross.Navigation;

    public class MessageViewModel : BaseViewModel
    {
        #region Constructor

        public MessageViewModel(IMvxNavigationService navigationService, ILocalStorageService localStorageService)
            : base(navigationService, localStorageService)
        {
        }

        #endregion
    }
}
