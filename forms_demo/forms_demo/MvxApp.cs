﻿namespace forms_demo
{
    using form_demo.Data.LocalService.Implementation;
    using form_demo.Data.LocalService.Interface;
    using form_demo.Data.Mapper;
    using forms_demo.ViewModels;
    using MvvmCross;
    using MvvmCross.IoC;
    using MvvmCross.ViewModels;

    public class MvxApp : MvxApplication
    {
        public static void InitializeIoC()
        {
            Mvx.IoCProvider.RegisterType<ILocalStorageService, LocalStorageService>();
        }
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Akavache.Registrations.Start("FormsDemo");
            AutoMapperConfig.RegisterMappings();
            InitializeIoC();

            RegisterAppStart<SplashViewModel>();
        }
    }
}
