﻿namespace forms_demo.Test.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using forms_demo.BaseTypes.Entities;
    using forms_demo.ViewModels;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class LoginViewModelTests : ViewModelTestBase
    {
        private LoginViewModel sut;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            sut = new LoginViewModel(navigationService, localStorageService);
        }

        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
            sut = null;
        }

        [Test]
        public void Constructor_WithNullNavigationService_ThrowsArgumentNullException()
        {
            //Assert
            Assert.Throws<ArgumentNullException>(() => new LoginViewModel(null, localStorageService));
        }

        [Test]
        public void Constructor_WithNullLocaleStorageService_ThrowsArgumentNullException()
        {
            //Assert
            Assert.Throws<ArgumentNullException>(() => new LoginViewModel(navigationService, null));
        }

        [Test]
        public async Task LoginCommand_CorrectCredentials_SavesUserAndNavigatesToHomeView()
        {
            //Arrange
            sut.UserName = "a";
            sut.Password = "a";

            //Act
            await sut.LoginCommand.ExecuteAsync();

            //Assert
            await localStorageService.ReceivedWithAnyArgs(1).CreateUserAsync(Arg.Any<User>());
            await navigationService.Received(1).Navigate<HomeViewModel>();
            await navigationService.Received(0).Navigate<MessageViewModel>();
        }

        [Test]
        public async Task LoginCommand_InvalidCredentials_NavigatesToErrorView()
        {
            //Arrange
            sut.UserName = "t";
            sut.Password = "a";

            //Act
            await sut.LoginCommand.ExecuteAsync();

            //Assert
            await localStorageService.ReceivedWithAnyArgs(0).CreateUserAsync(Arg.Any<User>());
            await navigationService.Received(0).Navigate<HomeViewModel>();
            await navigationService.Received(1).Navigate<MessageViewModel>();
        }
    }
}
