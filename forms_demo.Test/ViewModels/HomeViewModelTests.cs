﻿namespace forms_demo.Test.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using forms_demo.BaseTypes.Entities;
    using forms_demo.ViewModels;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class HomeViewModelTests : ViewModelTestBase
    {
        private User user;
        private HomeViewModel sut;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            user = Substitute.For<User>();
            sut = new HomeViewModel(navigationService, localStorageService);
        }

        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
            user = null;
            sut = null;
        }

        [Test]
        public void Constructor_WithNullNavigationService_ThrowsArgumentNullException()
        {
            //Assert
            Assert.Throws<ArgumentNullException>(() => new HomeViewModel(null, localStorageService));
        }

        [Test]
        public void Constructor_WithNullLocaleStorageService_ThrowsArgumentNullException()
        {
            //Assert
            Assert.Throws<ArgumentNullException>(() => new HomeViewModel(navigationService, null));
        }

        [Test]
        public async Task Initialize_CallsGetUser()
        {
            //Arrange
            user.Name = "a";
            user.IsAuthenticated = true;
            localStorageService.GetUserAsync().Returns(user);

            //Act
            await sut.Initialize();

            //Assert
            await localStorageService.Received(1).GetUserAsync();
            Assert.AreEqual(sut.User, user);
        }

        [Test]
        public async Task LogoutCommand_CallsDeleteAllUsers()
        {
            //Act
            await sut.LogoutCommand.ExecuteAsync();

            //Assert
            await localStorageService.Received(1).DeleteAllUserAsync();
            await navigationService.Received(1).Close(sut);
        }
    }
}
