﻿namespace forms_demo.Test.ViewModels
{
    using form_demo.Data.LocalService.Interface;
    using MvvmCross.Navigation;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class ViewModelTestBase
    {
        protected IMvxNavigationService navigationService;
        protected ILocalStorageService localStorageService;

        [SetUp]
        public virtual void Setup()
        {
            navigationService = Substitute.For<IMvxNavigationService>();
            localStorageService = Substitute.For<ILocalStorageService>();
        }

        [TearDown]
        public virtual void TearDown()
        {
            navigationService = null;
            localStorageService = null;
        }
    }
}
