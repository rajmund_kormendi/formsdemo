﻿namespace forms_demo.Test.ViewModels
{
    using System.Threading.Tasks;
    using forms_demo.BaseTypes.Entities;
    using forms_demo.ViewModels;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class SplashViewModelTests : ViewModelTestBase
    {
        private User user;
        private SplashViewModel sut;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            user = Substitute.For<User>();
            sut = new SplashViewModel(navigationService, localStorageService);
        }

        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
            user = null;
            sut = null;
        }

        [Test]
        public async Task ViewAppeared_CallsGetUser()
        {
            //Act
            sut.ViewAppeared();

            //Arrange
            await localStorageService.Received(1).GetUserAsync();
        }

        [Test]
        public async Task ViewAppeared_NoSavedUser_NavigatesToLoginView()
        {
            //Arrange
            user = null;
            localStorageService.GetUserAsync().Returns(user);

            //Act
            sut.ViewAppeared();

            //Assert
            await localStorageService.Received(1).GetUserAsync();
            await navigationService.Received(1).Navigate<LoginViewModel>();
        }

        [Test]
        public async Task ViewAppeared_HasSavedUser_NavigatesToHomeView()
        {
            //Arrange
            user.IsAuthenticated = true;
            localStorageService.GetUserAsync().Returns(user);

            //Act
            sut.ViewAppeared();

            //Assert
            await localStorageService.Received(1).GetUserAsync();
            await navigationService.Received(1).Navigate<HomeViewModel>();
        }
    }
}
