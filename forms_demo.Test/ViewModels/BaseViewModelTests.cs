﻿namespace forms_demo.Test.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using forms_demo.ViewModels;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class BaseViewModelTests : ViewModelTestBase
    {
        private BaseViewModel sut;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            sut = new BaseViewModel(navigationService, localStorageService);
        }

        [TearDown]
        public override void TearDown()
        {
            base.Setup();
            sut = null;
        }

        [Test]
        public void Constructor_WithNullNavigationService_ThrowsArgumentNullException()
        {
            //Assert
            Assert.Throws<ArgumentNullException>(() => new BaseViewModel(null, localStorageService));
        }

        [Test]
        public void Constructor_WithNullLocaleStorageService_ThrowsArgumentNullException()
        {
            //Assert
            Assert.Throws<ArgumentNullException>(() => new BaseViewModel(navigationService, null));
        }

        [Test]
        public async Task BackCommand_CallsNavigationClose()
        {
            //Act
            await sut.BackCommand.ExecuteAsync();

            //Assert
            await navigationService.Received(1).Close(sut);
        }
    }
}
