﻿namespace forms_demo.Test.LocalServices
{
    using System.Threading.Tasks;
    using form_demo.Data.LocalService.Implementation;
    using form_demo.Data.LocalService.Interface;
    using forms_demo.BaseTypes.Entities;
    using NSubstitute;
    using NUnit.Framework;

    [TestFixture]
    public class LocalStorageServiceTests
    {
        private ILocalStorageService localStorageService;
        private ILocalStorageService sut;
        private User user;

        [SetUp]
        public void Setup()
        {
            localStorageService = Substitute.For<ILocalStorageService>();
            user = Substitute.For<User>();
            sut = new LocalStorageService();
        }

        [TearDown]
        public void TearDown()
        {
            localStorageService = null;
            user = null;
            sut = null;
        }

        [Test]
        public async Task LocalStorageService_CreateUserAsync()
        {
            //Arrange
            user.Name = "test name";

            //Act
            await localStorageService.CreateUserAsync(user);

            //Assert
            await localStorageService.Received(1).CreateUserAsync(user);
        }

        [Test]
        public async Task LocalStorageService_GetUserAsync_ReturnsTheUser()
        {
            //Arrange
            user.Name = "test name";
            user.IsAuthenticated = true;

            localStorageService.GetUserAsync().Returns(user);

            //Act
            var result = await localStorageService.GetUserAsync();

            //Assert
            await localStorageService.Received(1).GetUserAsync();
            Assert.AreEqual(user, result);
        }

        [Test]
        public async Task LocalStorageService_DeleteUserAsync_DeletesTheUser()
        {
            //Act
            await localStorageService.DeleteAllUserAsync();

            //Assert
            await localStorageService.Received(1).DeleteAllUserAsync();
        }
    }
}
