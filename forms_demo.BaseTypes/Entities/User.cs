﻿namespace forms_demo.BaseTypes.Entities
{
    public class User
    {
        public string Name { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}
